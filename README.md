# Content Reminders

The Content Reminders module for Drupal is a tool that allows website
administrators to set up automatic reminders for specific pieces of content.
These reminders can be sent to a designated email address or set of comma-
separated email addresses. The module is designed to make it easy to keep track
of important content and ensure that it is reviewed or updated on a regular
basis. It can be used to send reminders for blog posts, news articles, or other
types of content that need to be kept fresh and up-to-date.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/content_reminders).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/content_reminders).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend
2. Configure the module by selecting which Content Types should allow Content
Reminders to be set at `admin/config/development/content_reminders`.
3. Configure permissions at `admin/people/permissions/module/content_reminders`.

## Maintainers

- Michael Kinnunen - [mkinnune](https://www.drupal.org/u/mkinnune)
- Iván Latorre - [netboss](https://www.drupal.org/u/netboss)
- Ari Hylas - [ari.saves](https://www.drupa.org/u/arisaves)