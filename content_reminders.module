<?php

/**
 * @file
 * Contains content_reminders.module.
 */

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_reminders\ContentReminderInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function content_reminders_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the content_reminders module.
    case 'help.page.content_reminders':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The module is designed to make it easy to keep track of important content and ensure that it is reviewed or updated on a regular basis. It can be used to send reminders for blog posts, news articles, or other types of content that need to be kept fresh and up-to-date.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_preprocess_HOOK() for HTML document templates.
 */
function content_reminders_preprocess_html(&$variables) {
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  if ($theme === 'adminimal_theme') {
    // Reference your custom stylesheet.
    $variables['#attached']['library'][] = "content_reminders/content_reminders";
  }
}

/**
 * Implements hook_form_alter().
 */
function content_reminders_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $settings = \Drupal::service('config.factory')
    ->get('content_reminders.settings');
  $allowed_types = $settings->get('content_types');

  if ($form_state->getFormObject() instanceof EntityFormInterface) {
    $entity = $form_state->getFormObject()->getEntity();
    $entity_type = $entity->bundle();
    $entity_id = $entity->id();
    $current_operation = $form_state->getFormObject()->getOperation();
    $disallowed_operations = [
      'delete', 'cancel', 'reset', 'layout_builder', 'replicate',
    ];
    if (in_array($entity_type, $allowed_types) && $entity_id && !in_array($current_operation, $disallowed_operations)) {
      \Drupal::service('content_reminders.content_reminder')
        ->addReminderToEntityForm($form, $form_state, $form_id, $entity_id);
    }
  }
}

/**
 * Implements hook_entity_delete().
 */
function content_reminders_entity_delete(EntityInterface $entity) {
  if ($entity instanceof NodeInterface) {
    $query = \Drupal::entityQuery('content_reminder')
      ->condition('nid', $entity->id());
    $results = $query->execute();
    if (count($results) > 0) {
      _content_reminders_delete_reminders($results);
    }
  }
}

/**
 * Implements hook_cron().
 */
function content_reminders_cron() {
  $time = time();
  $mailManager = \Drupal::service('plugin.manager.mail');
  $module = 'content_reminders';
  $key = 'content_reminder';
  $langcode = \Drupal::service('language_manager')->getDefaultLanguage()->getId();

  $storage = \Drupal::entityTypeManager()->getStorage('content_reminder');
  $query = \Drupal::entityQuery('content_reminder');
  $query->condition('date_time', $time, '<=')
    ->condition('status', TRUE);
  $matches = $query->execute();
  $entities = $storage->loadMultiple($matches);

  foreach ($entities as $entity) {
    if ($entity instanceof ContentReminderInterface) {
      $to = $entity->getEmails();
      $nid = $entity->getNodeId();
      $label = Node::load($nid)->label();
      $body = '<p>The content reminder from
        <a href="/node/' . $nid . '">' . $label . '</a>
        has the following message:</p>
        <p>' . $entity->getMessage() . '</p>';

      $params['message'] = $body;
      $params['nid'] = $nid;
      $params['node_title'] = $label;

      $result = $mailManager->mail($module, $key, $to, $langcode, $params);
      if ($result['result'] !== TRUE) {
        $message = t('There was a problem sending a content reminder: @reminder', ['@reminder' => $entity->id()]);
        \Drupal::logger('content_reminders')->error($message);
        return;
      }

      $message = t('The content reminder @reminder was sent to @emails', [
        '@reminder' => $entity->label(),
        '@emails' => $to,
      ]);
      \Drupal::logger('content_reminders')->notice($message);
      $entity->setStatus(FALSE);
      $entity->save();
    }
  }
}

/**
 * Implements hook_mail().
 */
function content_reminders_mail($key, &$message, $params) {
  $options = [
    'langcode' => $message['langcode'],
  ];

  if ($key == 'content_reminder') {
    $message['from'] = \Drupal::config('system.site')->get('mail');
    $message['subject'] = t('You have a content reminder for @title (node: @nid)', [
      '@title' => $params['node_title'],
      '@nid' => $params['nid'],
    ], $options);
    $message['body'][] = $params['message'];
  }
}

/**
 * Callback for the save button on the form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state object.
 *
 * @return \Drupal\Core\Ajax\AjaxResponse
 *   An ajax response.
 */
function _content_reminders_save(array $form, FormStateInterface $form_state) {
  return \Drupal::service('content_reminders.content_reminder')
    ->handleFormSubmit($form, $form_state);
}

/**
 * Function to delete content reminders based on their ids.
 *
 * @param array $reminder_ids
 *   Array of content reminder ids.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _content_reminders_delete_reminders(array $reminder_ids) {
  $storage = \Drupal::entityTypeManager()->getStorage('content_reminder');
  foreach ($reminder_ids as $id) {
    $reminder = $storage->load($id);
    $reminder->delete();
  }
}
