<?php

namespace Drupal\content_reminders;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides an interface for the Content Reminder Type.
 */
interface ContentReminderInterface extends ConfigEntityInterface {

  /**
   * The node id the reminder is for.
   *
   * @return string
   *   The node id for the content reminder.
   */
  public function getNodeId();

  /**
   * Sets the node id.
   *
   * @param string $nid
   *   The nid for the reminder.
   */
  public function setNodeId(string $nid);

  /**
   * The email addresses to send the reminder to.
   *
   * @return string
   *   The emails to be sent to.
   */
  public function getEmails();

  /**
   * Set the emails to send to.
   *
   * @param string $emails
   *   The emails submitted via the entity form.
   */
  public function setEmails(string $emails);

  /**
   * The date and time that the reminder should be sent.
   *
   * @return int
   *   The content reminder timestamp.
   */
  public function getDateTime();

  /**
   * Set the date and time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $dateTime
   *   The datetime submitted via the entity form.
   */
  public function setDateTime(DrupalDateTime $dateTime);

  /**
   * The optional note for the reminder.
   *
   * @return string
   *   The content reminder message.
   */
  public function getMessage();

  /**
   * Set the message.
   *
   * @param string $message
   *   The message submitted via the entity form.
   */
  public function setMessage(string $message);

}
