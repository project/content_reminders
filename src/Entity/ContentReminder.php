<?php

namespace Drupal\content_reminders\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\content_reminders\ContentReminderInterface;

/**
 * Defines the Content Reminder.
 *
 * @ConfigEntityType(
 *   id = "content_reminder",
 *   label = @Translation("Content reminder"),
 *   label_collection = @Translation("Content reminders"),
 *   label_singular = @Translation("Content reminder"),
 *   label_plural = @Translation("Content reminders"),
 *   label_count = @PluralTranslation(
 *     singular = "@count content reminder",
 *     plural = "@count content reminders",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\content_reminders\ContentReminderListBuilder",
 *     "form" = {
 *       "add" = "Drupal\content_reminders\Form\ContentReminderForm",
 *       "edit" = "Drupal\content_reminders\Form\ContentReminderForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "content_reminder",
 *   admin_permission = "administer content reminder",
 *   links = {
 *     "collection" = "/admin/structure/content_reminder",
 *     "add-form" = "/admin/structure/content_reminder/add",
 *     "preview-page" = "/admin/structure/content_reminder/{content_reminder}/preview",
 *     "edit-form" = "/admin/structure/content_reminder/{content_reminder}",
 *     "delete-form" = "/admin/structure/content_reminder/{content_reminder}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "nid",
 *     "emails",
 *     "date_time",
 *     "message"
 *   }
 * )
 */
class ContentReminder extends ConfigEntityBase implements ContentReminderInterface {

  /**
   * The content reminder id.
   *
   * @var string
   */
  protected $id;

  /**
   * The content reminder label.
   *
   * @var string
   */
  protected $label;

  /**
   * The content reminder status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The node id of the content reminder.
   *
   * @var string
   */
  protected $nid;

  /**
   * The comma-separated string of emails that the reminder should be sent to.
   *
   * @var string
   */
  protected $emails;

  /**
   * The timestamp for when the content reminder should be sent.
   *
   * @var int
   */
  protected $date_time;

  /**
   * The content reminder message.
   *
   * @var string
   */
  protected $message;

  /**
   * {@inheritdoc}
   */
  public function getNodeId() {
    return $this->nid;
  }

  /**
   * {@inheritdoc}
   */
  public function setNodeId($nid) {
    $this->nid = $nid;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmails() {
    return $this->emails;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmails(string $emails) {
    $this->emails = $emails;
  }

  /**
   * {@inheritdoc}
   */
  public function getDateTime() {
    return $this->date_time;
  }

  /**
   * {@inheritdoc}
   */
  public function setDateTime(DrupalDateTime $dateTime) {
    $this->date_time = $dateTime->getTimestamp();
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage(string $message) {
    $this->message = $message;
  }

}
