<?php

namespace Drupal\content_reminders\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_reminders\ContentReminderInterface;

/**
 * Content Reminder form.
 */
class ContentReminderForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);
    if (!$this->entity instanceof ContentReminderInterface) {
      return $form;
    }

    // Get the $node object to use it as the $form['nid'] #default_value.
    $node = '';
    if ($this->entity->getNodeId()) {
      // Load the node object.
      $node = $this->entityTypeManager->getStorage('node')->load($this->entity->getNodeId());
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the content reminder.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\content_reminders\Entity\ContentReminder::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['nid'] = [
      '#title' => $this->t('Node'),
      '#description' => $this->t('Start typing to find content.'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#default_value' => $node,
      '#required' => TRUE,
    ];

    $form['emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Who to email'),
      '#default_value' => $this->entity->getEmails(),
      '#description' => $this->t('Emails should be separated by a comma.'),
      '#required' => TRUE,
    ];

    $form['date_time'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date and Time'),
      '#default_value' => new DrupalDateTime('@' . $this->entity->getDateTime()),
      '#description' => $this->t('The date and time that the notification should be sent.'),
      '#required' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $this->entity->getMessage(),
      '#description' => $this->t('An optional note to send with the notification.'),
      '#required' => FALSE,
    ];

    // Set the form to rebuild. The submitted values are maintained in the
    // form state, and used to build the search results in the form definition.
    $form_state->setRebuild(TRUE);

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {
    $reminder = $this->getEntity();
    if (!$reminder instanceof ContentReminderInterface) {
      return parent::save($form, $form_state);
    }
    $reminder->setDateTime($form_state->getValue('date_time'));
    $result = $reminder->save();

    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new content reminder %label.', $message_args)
      : $this->t('Updated content reminder %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $result;
  }

}
