<?php

namespace Drupal\content_reminders;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Content Reminder Service.
 */
class ContentReminderService {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  private $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Function that attaches the reminder form to the node edit form.
   *
   * @param array $form
   *   The form that the Content Reminder form should be attached to.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   * @param string $form_id
   *   Form ID.
   * @param string|null $entity_id
   *   The entity_id that we are adding the form to.
   */
  public function addReminderToEntityForm(array &$form, FormStateInterface $form_state, $form_id, $entity_id) {

    $content_reminder = $this->getContentReminderFor($entity_id);
    if ($content_reminder && !$content_reminder instanceof ContentReminderInterface) {
      return;
    }

    $element['content_reminder'] = [
      '#type' => 'details',
      '#title' => $this->t('Content Reminder'),
      '#collapsed' => FALSE,
      '#collapsible' => TRUE,
      '#tree' => FALSE,
      '#weight' => 20,
      '#group' => 'advanced',
    ];

    $element['content_reminder']['cr_nid'] = [
      '#type' => 'hidden',
      '#value' => $entity_id,
    ];

    $element['content_reminder']['cr_emails'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Who to email'),
      '#default_value' => $content_reminder ? $content_reminder->getEmails() : '',
      '#description' => $this->t('Emails should be separated by a comma.'),
    ];

    $element['content_reminder']['cr_date_time'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Date and Time'),
      '#default_value' => $content_reminder ? new DrupalDateTime('@' . $content_reminder->getDateTime()) : '',
      '#description' => $this->t('The date and time that the notification should be sent.'),
    ];

    $element['content_reminder']['cr_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => $content_reminder ? $content_reminder->getMessage() : '',
      '#description' => $this->t('An optional note to send with the notification.'),
    ];

    // Currently handled with ajax. Maybe we attach another submit function to
    // the form in order to handle the submission instead? This would require
    // the node edit form to be saved in order to add a content reminder.
    $element['content_reminder']['cr_save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [
        '::submitForm',
        '::save',
      ],
      '#ajax' => [
        'callback' => '_content_reminders_save',
        'wrapper' => 'cr_output',
      ],
    ];

    $element['content_reminder']['output'] = [
      '#prefix' => '<div id="cr_output">',
      '#suffix' => '</div>',
    ];

    $form += $element;
  }

  /**
   * Handles the callback for saving on the node edit form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function handleFormSubmit(array $form, FormStateInterface $form_state) {
    $nid = $form_state->getValue('cr_nid');
    $emails = $form_state->getValue('cr_emails');
    $date_time = $form_state->getValue('cr_date_time');
    $message = $form_state->getValue('cr_message');

    $reminder = $this->getContentReminderFor($nid);
    if (!$reminder) {
      $reminder = $this->entityTypeManager
        ->getStorage('content_reminder')
        ->create();
      if ($reminder instanceof ContentReminderInterface) {
        $reminder->set('id', $nid . '_content_reminder');
        $reminder->set('label', $nid . ' content reminder');
        $reminder->setOriginalId($nid . '_content_reminder');
        $reminder->setNodeId($nid);
      }
    }
    if ($reminder instanceof ContentReminderInterface) {
      $reminder->set('status', 1);
      $reminder->setEmails($emails);
      $reminder->setDateTime($date_time);
      $reminder->setMessage($message);
      $reminder->save();
    }

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#cr_output', '<div id="cr_output" class="cr-output">Content Reminder Saved (<a href="/admin/structure/content_reminder" target="_blank">View all content reminders</a>)</div>'));
    $response->addCommand(new InvokeCommand('input[name="changed"]','val',[time()]));
    return $response;
  }

  /**
   * Get a content reminder for a node.
   *
   * @param string $entity_id
   *   The node id.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   A content reminder config entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getContentReminderFor(string $entity_id) {
    $content_reminder = NULL;
    $query = $this->entityTypeManager
      ->getStorage('content_reminder')
      ->getQuery();
    $result = $query->condition('nid', $entity_id)
      ->accessCheck(FALSE)
      ->execute();
    if ($result) {
      $key = key($result);
      $content_reminder = $this->entityTypeManager
        ->getStorage('content_reminder')
        ->load($key);
    }
    return $content_reminder;
  }

}
