<?php

namespace Drupal\content_reminders;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;

/**
 * List Builder class for the Content Reminders.
 */
class ContentReminderListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['node'] = $this->t('Node');
    $header['status'] = $this->t('Status');
    $header['nid'] = $this->t('Node Id');
    $header['emails'] = $this->t('Email Addresses');
    $header['date_time'] = $this->t('Date / Time');
    $header['message'] = $this->t('Message');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if (!$entity instanceof ContentReminderInterface) {
      return parent::buildRow($entity);
    }

    // Load the node to get its title.
    $node = Node::load($entity->getNodeId());
    $node_title = $node->title->value;

    $row['label'] = $entity->label();
    $row['node'] = Link::createFromRoute($node_title, 'entity.node.canonical', ['node' => $entity->getNodeId()]);
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['nid'] = $entity->getNodeId();
    $row['emails'] = $entity->getEmails();
    $row['date_time'] = new DrupalDateTime('@' . $entity->getDateTime());
    $row['message'] = $entity->getMessage();

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $operations['preview'] = [
      'title' => $this->t('Preview'),
      'weight' => 20,
      'url' => $this->ensureDestination($entity->toUrl('preview-page', [$entity->id()])),
    ];
    return $operations;
  }

}
