<?php

namespace Drupal\content_reminders\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\content_reminders\ContentReminderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Content Reminders.
 */
class ContentReminderController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Function to preview a content reminder.
   *
   * @param string $content_reminder
   *   The id of the content reminder.
   *
   * @return array
   *   Render array for the items in the content reminder.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function preview($content_reminder) {
    $entity = $this->entityTypeManager()
      ->getStorage('content_reminder')
      ->load($content_reminder);
    if (!$entity instanceof ContentReminderInterface) {
      return [];
    }

    $items = [
      'Label: ' => $entity->label(),
      'ID:' => $entity->id(),
      'Status' => $entity->status(),
      'Node Id:' => $entity->getNodeId(),
      'Emails:' => $entity->getEmails(),
      'Date/Time:' => new DrupalDateTime('@' . $entity->getDateTime()),
      'Message' => $entity->getMessage(),
    ];
    return [
      '#theme' => 'item_list',
      '#items' => $items,
    ];
  }

}
